const container = document.createElement("div");
fetch(`https://ajax.test-danit.com/api/swapi/films`)
    .then(data => data.json())
    .then(data => {
        data.forEach(({name,episodeId,openingCrawl,characters}) => {
            const filmItem = document.createElement("div");
                 filmItem.innerHTML = `<h1>${name}</h1><p>Episode №${episodeId}</p><p>${openingCrawl}</p>`;
                 container.append(filmItem);
            const promArr = characters.map(postUrl => {
                return fetch(postUrl)
                    .then(characters => characters.json())
            })
            Promise.all(promArr).then(characters=> {
                characters.map(({name})=>{
                const charactersDiv = document.createElement("ul");
                charactersDiv.innerHTML = `<li>${name}</li>`;
                filmItem.append(charactersDiv)
            })
            })
        })
    })
document.body.append(container);